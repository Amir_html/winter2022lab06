import java.util.Scanner;
public class ShutTheBox{
	public static void main(String[] args){
		Scanner reader= new Scanner(System.in);
		System.out.println("Welcome to our great board game!");
		Board player=new Board();
		boolean gameOver=false;
		int winPlayer1=0;
		int winPlayer2=0;
		while(!gameOver){
			System.out.println("Player 1's turn:");
			System.out.println(player);
			if(player.playATurn()){
				System.out.println("Player 2 wins!");
				winPlayer2++;
				System.out.println("Do you want to play again?"+"\n"+"Enter yes for continuing and something else to finish the game.");
				String again=reader.next();
				if(again.equals("yes")){
					 player=new Board();
					continue;
				}
				System.out.println("Player 1 won "+winPlayer1+" times"+"\n"+"Player 2 won "+winPlayer2+" times");
				gameOver=true;
			}else{
				System.out.println("Player 2's turn:");
				System.out.println(player);
				if(player.playATurn()){
					System.out.println("Player 1 wins!");
					winPlayer1++;
					System.out.println("Do you want to play again?"+"\n"+"Enter yes for continuing and something else to finish the game.");
					String again=reader.next();
					if(again.equals("yes")){
						 player=new Board();
						continue;
				}
					System.out.println("Player 1 won "+winPlayer1+" times"+"\n"+"Player 2 won "+winPlayer2+" times");
					gameOver=true;
				}
			}
		}
	}
}