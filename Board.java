public class Board{
	private Die die1;
	private Die die2;
	private boolean[] closedTiles;
	public Board(){
		this.die1=new Die();
		this.die2=new Die();
		this.closedTiles=new boolean[12];
	}
	public String toString(){
		String s="";
		for(int i=0;i<closedTiles.length;i++){
			if(closedTiles[i]){
				s=s+" "+"X";
			}else{
				s=s+" "+(i+1);
			}
		}
		return s;
	}
	public boolean playATurn(){
		this.die1.roll();
		this.die2.roll();
		System.out.println("Object 1: "+this.die1.toString()+"\n"+"Object 2: "+this.die2.toString());
		int sum=this.die1.getPips()+this.die2.getPips();
		if(closedTiles[sum-1]){
			System.out.println("This position is already shut.");
			return true;
		}else{
			closedTiles[sum-1]=true;
			System.out.println("Closing title: "+sum);
			return false;
		}
	}
}